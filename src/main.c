#include <stdio.h>
#include <stdlib.h>
#include "Components/Memory/memory.h"
#include "Components/Processor/processor.h"
#include "Components/Processor/Instructions/instruction_implementations.h"

#define ROM_NAME "ctfrom"
#define BOOT_ROM "bootrom"

int handleError() {
    printf("Error!\n");
    exit(1);
}

int main() {
    printf("Initializing memory... ");
    memory_init() || handleError();
    printf("Success!\n");

    printf("Loading Boot ROM... ");
    memory_loadRom(BOOT_ROM, 0x0000) || handleError();
    printf("Success!\n");

    printf("Loading ROM \"%s\"... ", ROM_NAME);
    memory_loadRom(ROM_NAME, 0x0597) || handleError();
    printf("Success!\n");

    printf("Initializing processor... ");
    processor_init() || handleError();
    printf("Success!\n");

    printf("\n");
    while(running)
        processor_step();

    impl_clrscr();

    printf("\n");

    printf("Stopping processor... ");
    processor_stop() || handleError();
    printf("Success!\n");

    printf("Stopping memory... ");
    memory_stop() || handleError();
    printf("Success!\n");
    return 0;
}
