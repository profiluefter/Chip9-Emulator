//
// Created by Fabian on 17.12.2019.
//

#ifndef CHIP9_EMULATOR_PROCESSOR_H
#define CHIP9_EMULATOR_PROCESSOR_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef uint8_t reg_t;
typedef uint16_t pair_t;

reg_t *reg_a, *reg_b, *reg_c, *reg_d, *reg_e, *reg_h, *reg_l;
pair_t *reg_bc, *reg_de, *reg_hl;

uint16_t *sp, *pc;
uint8_t *flags;
//      76543210
//FLAGS:ZNHC0000

bool running;

bool processor_init();
bool processor_stop();

void processor_step();

#endif //CHIP9_EMULATOR_PROCESSOR_H
