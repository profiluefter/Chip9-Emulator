//
// Created by Fabian on 17.12.2019.
//

#ifndef CHIP9_EMULATOR_INSTRUCTIONS_H
#define CHIP9_EMULATOR_INSTRUCTIONS_H

#include <stdbool.h>
#include <stdint.h>

typedef int (*operation_t)();

int executeInstruction();


#endif //CHIP9_EMULATOR_INSTRUCTIONS_H
