//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include <stdio.h>
#include <stdlib.h>

int hcf() {
    if (DEBUG)
        printf("FIRE!");
    running = false;
    return 1;
}