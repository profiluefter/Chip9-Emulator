//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int sub() {
    impl_subtract(get_register_by_offset(0x8));
    return 1;
}

int subi() {
    INS_DEF;
    impl_subtractValue(oi.opcode + sizeof(memory_t));
    return 2;
}

void impl_subtract(reg_t *reg) {
    (*reg) -= *reg_a;
    impl_setFlag('Z', *reg == 0);
    impl_setFlag('N', (*reg & 0x80) == 0x80);
    impl_setFlag('H', ((*reg & 0xF) - (*reg_a & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg - ((uint16_t) *reg_a) & 0x100) == 0x100);
}

void impl_subtractValue(const memory_t *value) {
    (*reg_a) -= *value;
    impl_setFlag('Z', *reg_a == 0);
    impl_setFlag('N', (*reg_a & 0x80) == 0x80);
    impl_setFlag('H', ((*reg_a & 0xF) - (*value & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg_a - (uint16_t) *value & 0x100) == 0x100);
}