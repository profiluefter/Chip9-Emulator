//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"
#include <stdio.h>

int inc() {
    impl_increment(get_register_by_offset(0x0));
    return 1;
}

int inx() {
    impl_incrementPair(get_pair_by_offset(0xA));
    return 1;
}

void impl_increment(reg_t* reg) {
    (*reg)++;
    impl_setFlag('Z', *reg == 0);
    impl_setFlag('N',(*reg & 0x80) == 0x80);
    impl_setFlag('H', ((*reg & 0xF) + (1 & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg + ((uint16_t) 1) & 0x100) == 0x100);
}

//NO FLAGS!
void impl_incrementPair(pair_t* reg) {
    (*reg)++;
}