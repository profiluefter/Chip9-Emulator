//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"
#include "../../../Memory/memory.h"
#include <stdio.h>

int add() {
    impl_add(get_register_by_offset(0x0));
    return 1;
}

int addi() {
    INS_DEF;
    impl_addValue(oi.opcode + sizeof(memory_t));
    return 2;
}

int addx() {
    impl_addPair(get_pair_by_offset(0x8));
    return 1;
}

void impl_add(reg_t *reg) {
    *reg += *reg_a;
    impl_setFlag('Z', *reg == 0);
    impl_setFlag('N', (*reg & 0x80) == 0x80);
    impl_setFlag('H', ((*reg & 0xF) + (*reg_a & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg + ((uint16_t) *reg_a) & 0x100) == 0x100);
}

void impl_addValue(const memory_t *value) {
    *reg_a += *value;
    impl_setFlag('Z', *reg_a == 0);
    impl_setFlag('N', (*reg_a & 0x80) == 0x80);
    impl_setFlag('H', ((*value & 0xF) + (*reg_a & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *value + (uint16_t) *reg_a & 0x100) == 0x100);
}

void impl_addPair(pair_t *reg) {
    *reg += *reg_a;
    impl_setFlag('Z', *reg == 0);
    impl_setFlag('N', (*reg & 0x8000) == 0x8000);
    impl_setFlag('H', ((*reg & 0xF) + (*reg_a & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', (((uint16_t) *reg) + ((uint16_t) *reg_a) & 0x100) == 0x100);
}