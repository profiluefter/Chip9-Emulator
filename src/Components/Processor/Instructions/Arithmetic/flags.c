//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int clrflag() {
    impl_clearFlags();
    return 1;
}

int setflag() {
    INS_DEF;
    switch (oi.opcode[0]) {
        case 18:
            impl_setFlag('Z', true);
            break;
        case 28:
            impl_setFlag('Z', false);
            break;
        case 38:
            impl_setFlag('N', true);
            break;
        case 48:
            impl_setFlag('N', false);
            break;
        case 58:
            impl_setFlag('H', true);
            break;
        case 68:
            impl_setFlag('H', false);
            break;
        case 78:
            impl_setFlag('C', true);
            break;
        case 88:
            impl_setFlag('C', false);
            break;
    }
    return 1;
}

void impl_clearFlags() {
    *flags = 0;
}

void impl_setFlag(char flag, bool x) {
    switch (flag) { // NOLINT(hicpp-multiway-paths-covered)
        case 'Z':
            if (x)
                (*flags) |= 0x80;
            else
                (*flags) &= 0x7F;
            break;
        case 'N':
            if (x)
                (*flags) |= 0x40;
            else
                (*flags) &= 0xBF;
            break;
        case 'H':
            if (x)
                (*flags) |= 0x20;
            else
                (*flags) &= 0xDF;
            break;
        case 'C':
            if (x)
                (*flags) |= 0x10;
            else
                (*flags) &= 0xEF;
            break;
    }
}

bool impl_getFlag(char flag) {
    switch (flag) { // NOLINT(hicpp-multiway-paths-covered)
        case 'Z':
            return ((*flags) & 0x80) == 0x80;
        case 'N':
            return ((*flags) & 0x40) == 0x40;
        case 'H':
            return ((*flags) & 0x20) == 0x20;
        case 'C':
            return ((*flags) & 0x10) == 0x10;
    }
}
