//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int dec() {
    impl_decrement(get_register_by_offset(0x0));
    return 1;
}

void impl_decrement(reg_t* reg) {
    (*reg)--;
    impl_setFlag('Z', *reg == 0);
    impl_setFlag('N',(*reg & 0x80) == 0x80);
    impl_setFlag('H', ((*reg & 0xF) - (1 & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg - ((uint16_t) 1) & 0x100) == 0x100);
}