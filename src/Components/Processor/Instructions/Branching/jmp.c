//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"
#include <stdio.h>

int jmp() {
    return impl_jump(get_two_args());
}

int jmpz() {
    return impl_jmpz(get_two_args());
}

int jmpnz() {
    return impl_jmpnz(get_two_args());
}

int jmpn() {
    return impl_jmpn(get_two_args());
}

int jmpnn() {
    return impl_jmpnn(get_two_args());
}

int jmph() {
    return impl_jmph(get_two_args());
}

int jmpnh() {
    return impl_jmpnh(get_two_args());
}

int jmpc() {
    return impl_jmpc(get_two_args());
}

int jmpnc() {
    return impl_jmpnc(get_two_args());
}

int impl_jump(pair_t addr) {
    *pc = addr;
    return 0;
}

int impl_jmpz(pair_t addr) {
    if (impl_getFlag('Z'))
        return impl_jump(addr);
    return 3;
}

int impl_jmpnz(pair_t addr) {
    if (!impl_getFlag('Z'))
        return impl_jump(addr);
    return 3;
}

int impl_jmpn(pair_t addr) {
    if (impl_getFlag('N'))
        return impl_jump(addr);
    return 3;
}

int impl_jmpnn(pair_t addr) {
    if (!impl_getFlag('N'))
        return impl_jump(addr);
    return 3;
}

int impl_jmph(pair_t addr) {
    if (impl_getFlag('H'))
        return impl_jump(addr);
    return 3;
}

int impl_jmpnh(pair_t addr) {
    if (!impl_getFlag('H'))
        return impl_jump(addr);
    return 3;
}

int impl_jmpc(pair_t addr) {
    if (impl_getFlag('C'))
        return impl_jump(addr);
    return 3;
}

int impl_jmpnc(pair_t addr) {
    if (!impl_getFlag('C'))
        return impl_jump(addr);
    return 3;
}
