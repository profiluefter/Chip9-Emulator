//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int call() {
    impl_pushPairValue((*pc)+(sizeof(memory_t)*3));
    *pc = get_two_args();
    return 0;
}

int ret() {
    impl_popPair(pc);
    return 0;
}