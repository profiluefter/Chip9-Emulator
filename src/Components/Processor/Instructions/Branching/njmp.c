//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int njmp() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMP\n");
    INS_DEF;
    impl_jump((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpz() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPZ\n");
    INS_DEF;
    impl_jmpz((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpnz() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPNZ\n");
    INS_DEF;
    impl_jmpnz((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpn() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPN\n");
    INS_DEF;
    impl_jmpn((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpnn() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPNN\n");
    INS_DEF;
    impl_jmpnn((*pc) + (oi.opcode[1]));
    return 2;
}

int njmph() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPH\n");
    INS_DEF;
    impl_jmph((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpnh() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPNH\n");
    INS_DEF;
    impl_jmpnh((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpc() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPC\n");
    INS_DEF;
    impl_jmpc((*pc) + (oi.opcode[1]));
    return 2;
}

int njmpnc() {
    if(DEBUG_JUMP)
        printf("[EMU] NJMPNC\n");
    INS_DEF;
    impl_jmpnc((*pc) + (oi.opcode[1]));
    return 2;
}
