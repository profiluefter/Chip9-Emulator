//
// Created by Fabian on 17.12.2019.
//

#include "instructions.h"
#include "instruction_stubs.h"
#include "../../Memory/memory.h"
#include <stdio.h>

static operation_t operations[] = {
        /*Memory*/
        /*LDI*/ [0x20] = ldi, [0x30] = ldi, [0x40] = ldi, [0x50] = ldi, [0x60] = ldi, [0x70] = ldi, [0x80] = ldi, [0x90] = ldi,
        /*LDX*/ [0x21] = ldx, [0x31] = ldx, [0x41] = ldx, [0x22] = ldx,
        /*PUSH*/[0x81] = push, [0x91] = push, [0xA1] = push, [0xB1] = push, [0xC1] = push, [0xD1] = push, [0xC0] = push, [0xD0] = push, [0x51] = push, [0x61] = push, [0x71] = push,
        /*POP*/ [0x82] = pop, [0x92] = pop, [0xA2] = pop, [0xB2] = pop, [0xC2] = pop, [0xD2] = pop, [0xC3] = pop, [0xD3] = pop, [0x52] = pop, [0x62] = pop, [0x72] = pop,
        /*MOV1*/ [0x09] = mov, [0x19] = mov, [0x29] = mov, [0x39] = mov, [0x49] = mov, [0x59] = mov, [0x69] = mov, [0x79] = mov, [0x89] = mov, [0x99] = mov, [0xA9] = mov, [0xB9] = mov, [0xC9] = mov, [0xD9] = mov, [0xE9] = mov, [0xF9] = mov,
        /*MOV2*/ [0x0A] = mov, [0x1A] = mov, [0x2A] = mov, [0x3A] = mov, [0x4A] = mov, [0x5A] = mov, [0x6A] = mov, [0x7A] = mov, [0x8A] = mov, [0x9A] = mov, [0xAA] = mov, [0xBA] = mov, [0xCA] = mov, [0xDA] = mov, [0xEA] = mov, [0xFA] = mov,
        /*MOV3*/ [0x0B] = mov, [0x1B] = mov, [0x2B] = mov, [0x3B] = mov, [0x4B] = mov, [0x5B] = mov, [0x6B] = mov, [0x7B] = mov, [0x8B] = mov, [0x9B] = mov, [0xAB] = mov, [0xBB] = mov, [0xCB] = mov, [0xDB] = mov, [0xEB] = mov, [0xFB] = mov,
        /*MOV4*/ [0x0C] = mov, [0x1C] = mov, [0x2C] = mov, [0x3C] = mov, [0x4C] = mov, [0x5C] = mov, /*6C = hcf,*/ [0x7C] = mov, [0x8C] = mov, [0x9C] = mov, [0xAC] = mov, [0xBC] = mov, [0xCC] = mov, [0xDC] = mov, [0xEC] = mov, [0xFC] = mov,
        /*MOV5*/ [0xED] = mov, [0xFD] = mov,

        /*Arithmetic*/
        /*CLRFLAG*/ [0x08] = clrflag,
        /*SETFLAG*/ [0x18] = setflag, [0x28] = setflag, [0x38] = setflag, [0x48] = setflag, [0x58] = setflag, [0x68] = setflag, [0x78] = setflag, [0x88] = setflag,
        /*ADD*/ [0x04] = add, [0x14] = add, [0x24] = add, [0x34] = add, [0x44] = add, [0x54] = add, [0x64] = add, [0x74] = add,
        /*ADDI*/ [0xA7] = addi,
        /*ADDX*/ [0x83] = addx, [0x93] = addx, [0xA3] = addx,
        /*SUB*/ [0x84] = sub, [0x94] = sub, [0xA4] = sub, [0xB4] = sub, [0xC4] = sub, [0xD4] = sub, [0xE4] = sub, [0xF4] = sub,
        /*SUBI*/ [0xB7] = subi,
        /*INC*/ [0x03] = inc, [0x13] = inc, [0x23] = inc, [0x33] = inc, [0x43] = inc, [0x53] = inc, [0x63] = inc, [0x73] = inc,
        /*INX*/ [0xA8] = inx, [0xB8] = inx, [0xC8] = inx,
        /*DEC*/ [0x07] = dec, [0x17] = dec, [0x27] = dec, [0x37] = dec, [0x47] = dec, [0x57] = dec, [0x67] = dec, [0x77] = dec,

        /*Logical*/
        /*AND*/ [0x05] = and, [0x15] = and, [0x25] = and, [0x35] = and, [0x45] = and, [0x55] = and, [0x65] = and, [0x75] = and,
        /*ANDI*/ [0xC7] = andi,
        /*OR*/ [0x85] = or, [0x95] = or, [0xA5] = or, [0xB5] = or, [0xC5] = or, [0xD5] = or, [0xE5] = or, [0xF5] = or,
        /*ORI*/ [0xD7] = ori,
        /*XOR*/ [0x06] = xor, [0x16] = xor, [0x26] = xor, [0x36] = xor, [0x46] = xor, [0x56] = xor, [0x66] = xor, [0x76] = xor,
        /*XORI*/[0xE7] = xori,

        /*Comparisons*/
        /*CMP*/ [0x86] = cmp, [0x96] = cmp, [0xA6] = cmp, [0xB6] = cmp, [0xC6] = cmp, [0xD6] = cmp, [0xE6] = cmp, [0xF6] = cmp,
        /*CMPI*/[0xF7] = cmpi,
        /*CMPS*/[0x0D] = cmps, [0x1D] = cmps, [0x2D] = cmps, [0x3D] = cmps, [0x4D] = cmps, [0x5D] = cmps, [0x6D] = cmps, [0x7D] = cmps,

        /* I/O */
        /*SIN*/[0xE0] = sin_nolib,
        /*SOUT*/[0xE1] = sout,
        /*CLRSCR*/[0xF0] = clrscr,
        /*DRAW*/[0xF1] = draw,

        /* Branching */
        /*JMP*/[0x0F] = jmp,

        /*JMPZ*/[0x1F] = jmpz,
        /*JMPNZ*/[0x2F] = jmpnz,
        /*JMPN*/[0x3F] = jmpn,
        /*JMPNN*/[0x4F] = jmpnn,
        /*JMPH*/[0x5F] = jmph,
        /*JMPNH*/[0x6F] = jmpnh,
        /*JMPC*/[0x7F] = jmpc,
        /*JMPNC*/[0x8F] = jmpnc,

        /*NJMP*/[0x9F] = njmp,

        /*NJMPZ*/[0xAF] = njmpz,
        /*NJMPNZ*/[0xBF] = njmpnz,
        /*NJMPN*/[0xCF] = njmpn,
        /*NJMPNN*/[0xDF] = njmpnn,
        /*NJMPH*/[0xEF] = njmph,
        /*NJMPNH*/[0xFF] = njmpnh,
        /*NJMPC*/[0xEE] = njmpc,
        /*NJMPNC*/[0xFE] = njmpnc,

        /*CALL*/[0x1E] = call,
        /*RET*/[0x0E] = ret,

        /* Misc */
        /*NOP*/[0x00] = nop,
        /*HCF*/[0x6C] = hcf
};

int executeInstruction() {
    memory_t opcode = memory[*pc];

#if DEBUG
    static int i = 0;
    if(opcode == 0xF1){
        printf("[DRAW] %d\n", i++);
    }
#endif

    return operations[opcode]();
}
