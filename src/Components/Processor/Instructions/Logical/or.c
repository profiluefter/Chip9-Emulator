//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int or() {
    impl_or(get_register_by_offset(0x8));
    return 1;
}

int ori() {
    INS_DEF;
    impl_orValue(oi.opcode + sizeof(memory_t));
    return 2;
}

void impl_or(reg_t *reg) {
    *reg |= *reg_a;
    impl_setFlag('Z', *reg == 0);
    impl_setFlag('N',(*reg & 0x80) == 0x80);
    impl_setFlag('H',false);
    impl_setFlag('C',false);
}

void impl_orValue(const memory_t *value) {
    *reg_a |= *value;
    impl_setFlag('Z', *reg_a == 0);
    impl_setFlag('N',(*reg_a & 0x80) == 0x80);
    impl_setFlag('H',false);
    impl_setFlag('C',false);
}
