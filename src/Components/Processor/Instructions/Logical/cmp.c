//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"
#include <stdio.h>

int cmp() {
    impl_compare(get_register_by_offset(0x8));
    return 1;
}

int cmpi() {
    INS_DEF;
    impl_compareValue(oi.opcode + sizeof(memory_t));
    return 2;
}

int cmps() {
    impl_compareSigned(get_register_by_offset(0x0));
    return 1;
}

//TODO: check higher/lower
//TODO: potential error
void impl_compare(const reg_t *reg) {
    if (DEBUG)
        printf("[EMU] NORMAL COMPARE\n");
    impl_setFlag('Z', *reg == *reg_a);
    impl_setFlag('N', *reg - *reg_a > 0);
    impl_setFlag('H', ((*reg & 0xF) - (*reg_a & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg - ((uint16_t) *reg_a) & 0x100) == 0x100);
}

//TODO: check higher/lower
//TODO: potential error
void impl_compareValue(const memory_t *value) {
    if (DEBUG)
        printf("[EMU] COMPARE VALUE\n");
    impl_setFlag('Z', *value == *reg_a);
    impl_setFlag('N', *reg_a - *value > 0);
    impl_setFlag('H', ((*reg_a & 0xF) - (*value & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((uint16_t) *reg_a - (uint16_t) *value & 0x100) == 0x100);
}

//TODO: check higher/lower
//TODO: potential error
void impl_compareSigned(const reg_t *reg) {
    if (DEBUG)
        printf("[EMU] COMPARE SIGNED\n");
    impl_setFlag('Z', ((int8_t) *reg) == ((int8_t) *reg_a));
    impl_setFlag('N', ((int8_t) *reg) > ((int8_t) *reg_a));
    impl_setFlag('H', (((int8_t) *reg & 0xF) - ((int8_t) *reg_a & 0xF) & 0x10) == 0x10);
    impl_setFlag('C', ((int16_t) *reg - (int16_t) *reg_a & 0x100) == 0x100);
}
