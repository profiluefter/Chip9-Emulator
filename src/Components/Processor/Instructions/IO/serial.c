//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"
#include <stdio.h>

int sin_nolib() {
    impl_serialIn();
    return 1;
}

int sout() {
    impl_serialOut();
    return 1;
}

void impl_serialIn() {
    printf("[EMU] READ SERIAL\n");
    //TODO: Read console
}

void impl_serialOut() {
    printf("%c", *reg_a);
}
