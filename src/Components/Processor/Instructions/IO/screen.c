//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"
#include <stdio.h>

int clrscr() {
    impl_clrscr();
    return 1;
}

int draw() {
    impl_draw();
    return 1;
}

bool screen[64][128];

void impl_clrscr() {
    printf("\n\n[EMU] SCREEN PRINT BEGIN\n");

    for (int i = 0; i < 64; ++i) {
        for (int j = 0; j < 128; ++j) {
            if (screen[i][j])
                printf("+");
            else
                printf(" ");
            screen[i][j] = false;
        }
        printf("\n");
    }

    printf("\n[EMU] SCREEN PRINT STOP\n\n");
    if (DEBUG)
        printf("[EMU] CLEAR SCREEN\n");
    //TODO Implement screen
}

void impl_draw() {
    if (DEBUG)
        printf("[EMU] DRAW\n");
    //TODO Implement screen
    int startX = (int8_t) *reg_c;
    int startY = (int8_t) *reg_b;
    int bits = *reg_a;

    if (startX < 0) {
        bits = bits << startX;
        startX = 0;
    }

    if(startY < 0 || startY > 63)
        return;

    screen[startY][startX] = ((bits >> 7) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 6) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 5) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 4) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 3) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 2) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 1) & 0x1) == 0x1;
    screen[startY][startX] = ((bits >> 0) & 0x1) == 0x1;
}
