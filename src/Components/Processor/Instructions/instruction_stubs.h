//
// Created by fabian on 18.12.2019.
//

#ifndef CHIP9_EMULATOR_INSTRUCTION_STUBS_H
#define CHIP9_EMULATOR_INSTRUCTION_STUBS_H

#include "instruction_util.h"

#define DEBUG false
#define DEBUG_JUMP false

//Returns: Number of bytes used

/* Memory */
int ldi();
int ldx();

int push();
int pop();

int mov();

/* Arithmetic */
int clrflag();
int setflag();

int add();
int addi();
int addx();

int sub();
int subi();

int inc();
int inx();

int dec();

/* Logical*/
int and();
int andi();

int or();
int ori();

int xor();
int xori();

int cmp();
int cmpi();
int cmps();

/* I/O */
int sin_nolib();
int sout();

int clrscr();
int draw();

/* Branching */
int jmp();

int jmpz();
int jmpnz();
int jmpn();
int jmpnn();
int jmph();
int jmpnh();
int jmpc();
int jmpnc();

int njmp();

int njmpz();
int njmpnz();
int njmpn();
int njmpnn();
int njmph();
int njmpnh();
int njmpc();
int njmpnc();

int call();
int ret();

/* Miscellaneous */
int nop();
int hcf();

#endif //CHIP9_EMULATOR_INSTRUCTION_STUBS_H
