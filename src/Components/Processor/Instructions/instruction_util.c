//
// Created by fabian on 18.12.2019.
//

#include "instruction_util.h"
#include "../../Memory/memory.h"
#include "../processor.h"
#include <stdio.h>

struct opInfo getOpInfo() {
    struct opInfo info;
    info.opcode = &memory[*pc];
    return info;
}

reg_t *get_register_by_offset(int offset) {
    int regNumber = ((getOpInfo().opcode[0] & 0xF0) >> 4) - offset;
    switch (regNumber) {
        case 0:
            return reg_b;
        case 1:
            return reg_c;
        case 2:
            return reg_d;
        case 3:
            return reg_e;
        case 4:
            return reg_h;
        case 5:
            return reg_l;
        case 6:
            return memory+*reg_hl;
        case 7:
            return reg_a;
        default:
            printf("UNKNOWN REG NUMBER: %d",regNumber);
            return NULL;
    }
}

pair_t *get_pair_by_offset(int offset) {
    int regNumber = ((getOpInfo().opcode[0] & 0xF0) >> 4) - offset;
    switch (regNumber) {
        case 0:
            return reg_bc;
        case 1:
            return reg_de;
        case 2:
            return reg_hl;
        default:
            printf("UNKNOWN PAIR NUMBER: %d",regNumber);
            return NULL;
    }
}

pair_t get_two_args() {
    struct opInfo info = getOpInfo();
    int right = info.opcode[1];
    int left = info.opcode[2];
    left = left << 8;
    return left + right;
}