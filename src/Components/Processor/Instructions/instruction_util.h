//
// Created by fabian on 18.12.2019.
//

#ifndef CHIP9_EMULATOR_INSTRUCTION_UTIL_H
#define CHIP9_EMULATOR_INSTRUCTION_UTIL_H

#include <stdint.h>
#include "../processor.h"

struct opInfo {
    uint8_t *opcode;
};

struct opInfo getOpInfo();
reg_t *get_register_by_offset(int offset);
pair_t *get_pair_by_offset(int offset);
pair_t get_two_args();

#define INS_DEF struct opInfo oi = getOpInfo()

#endif //CHIP9_EMULATOR_INSTRUCTION_UTIL_H
