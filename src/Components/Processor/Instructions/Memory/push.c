//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../instruction_implementations.h"
#include "../../processor.h"
#include "../../../Memory/memory.h"

int push() {
    INS_DEF;
    switch (oi.opcode[0]) {
        case 0xC0:
            impl_push(memory + *reg_hl);
            break;
        case 0xD0:
            impl_push(reg_a);
            break;
        case 0x51:
        case 0x61:
        case 0x71:
            impl_pushPair(get_pair_by_offset(0x5));
            break;
        default:
            impl_push(get_register_by_offset(0x8));
    }
    return 1;
}

void impl_push(const reg_t *reg) {
    impl_pushValue(*reg);
}

void impl_pushValue(reg_t value) {
    memory[*sp] = value;
    *sp -= sizeof(uint16_t);
}

void impl_pushPair(const pair_t *reg) {
    impl_pushPairValue(*reg);
}

void impl_pushPairValue(pair_t value) {
    memory[*sp] = value;
    *sp -= sizeof(uint16_t);
}
