//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../../../Memory/memory.h"
#include "../instruction_implementations.h"

int mov() {
    INS_DEF;

    if (oi.opcode[0] == 0xED) {
        impl_movePair(reg_hl, reg_bc);
        return 1;
    } else if (oi.opcode[0] == 0xFD) {
        impl_movePair(reg_hl, reg_de);
        return 1;
    }

    int8_t op1 = oi.opcode[0] >> 4;
    int8_t op2 = oi.opcode[0] & 0x0F;

    reg_t* src = NULL;  //Default values that should not be used
    reg_t* dest = NULL;

    switch (op1 % 8) {
        case 0: src = reg_b; break;
        case 1: src = reg_c; break;
        case 2: src = reg_d; break;
        case 3: src = reg_e; break;
        case 4: src = reg_h; break;
        case 5: src = reg_l; break;
        case 6: src = memory+(*reg_hl); break;
        case 7: src = reg_a; break;
    }

    op2 -= 0x9;

    if(op1 / 8 == 0)
        switch (op2 % 8) {
            case 0: dest = reg_b; break;
            case 1: dest = reg_d; break;
            case 2: dest = reg_h; break;
            case 3: dest = memory+(*reg_hl); break;
        }
    else
        switch (op2 % 8) {
            case 0: dest = reg_c; break;
            case 1: dest = reg_e; break;
            case 2: dest = reg_l; break;
            case 3: dest = reg_a; break;
        }

    impl_move(dest, src);

    return 1;
}

void impl_move(reg_t *dest, const reg_t *src) {
    *dest = *src;
}

void impl_movePair(pair_t *dest, const pair_t *src) {
    *dest = *src;
}