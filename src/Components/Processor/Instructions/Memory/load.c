//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../../processor.h"
#include "../instruction_implementations.h"

int ldi() {
    INS_DEF;
    impl_load(get_register_by_offset(2), oi.opcode[1]);
    return 2;
}

int ldx() {
    INS_DEF;
    switch (oi.opcode[0]) {
        case 0x21:
            impl_loadPair(reg_bc, get_two_args());
            break;
        case 0x31:
            impl_loadPair(reg_de, get_two_args());
            break;
        case 0x41:
            impl_loadPair(reg_hl, get_two_args());
            break;
        case 0x22:
            impl_loadPair(sp, get_two_args());
            break;
    }
    return 3;
}

void impl_load(reg_t *reg, reg_t value) {
    *reg = value;
}

void impl_loadPair(pair_t *reg, pair_t value) {
    *reg = value;
}
