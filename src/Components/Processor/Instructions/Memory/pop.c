//
// Created by Fabian on 17.12.2019.
//

#include "../instruction_stubs.h"
#include "../instruction_implementations.h"
#include "../../processor.h"
#include "../../../Memory/memory.h"

int pop() {
    INS_DEF;
    switch (oi.opcode[0]) {
        case 0xC3:
            impl_pop(memory + *reg_hl);
            break;
        case 0xD3:
            impl_pop(reg_a);
            break;
        case 0x52:
        case 0x62:
        case 0x72:
            impl_popPair(get_pair_by_offset(0x5));
            break;
        default:
            impl_push(get_register_by_offset(0x8));
    }
    return 1;
}

void impl_pop(reg_t *reg) {
    *sp += 0x2;
    *reg = memory[*sp];
}

void impl_popPair(pair_t *reg) {
    *sp += 0x2;
    *reg = memory[*sp];
}
