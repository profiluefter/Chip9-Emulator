//
// Created by Fabian on 18.12.2019.
//

#ifndef CHIP9_EMULATOR_INSTRUCTION_IMPLEMENTATIONS_H
#define CHIP9_EMULATOR_INSTRUCTION_IMPLEMENTATIONS_H

#include "../../Memory/memory.h"

/* Memory */

void impl_load(reg_t *reg, reg_t value);
void impl_loadPair(pair_t *reg, pair_t value);

void impl_push(const reg_t *reg);
void impl_pushPair(const pair_t *reg);

void impl_pushValue(reg_t value);
void impl_pushPairValue(pair_t value);

void impl_pop(reg_t *reg);
void impl_popPair(pair_t *reg);

void impl_move(reg_t *dest, const reg_t *src);
void impl_movePair(pair_t *dest, const pair_t *src);

/* Arithmetic */

void impl_clearFlags();
void impl_setFlag(char flag, bool x);
bool impl_getFlag(char flag);

void impl_add(reg_t* reg);
void impl_addValue(const memory_t* value);
void impl_addPair(pair_t* reg);

void impl_subtract(reg_t* reg);
void impl_subtractValue(const memory_t* value);

void impl_increment(reg_t* reg);
void impl_incrementPair(pair_t* reg);

void impl_decrement(reg_t* reg);

/* Logic */

void impl_and(reg_t *reg);
void impl_andValue(const memory_t *value);

void impl_or(reg_t *reg);
void impl_orValue(const memory_t *value);

void impl_xor(reg_t *reg);
void impl_xorValue(const memory_t *value);

void impl_compare(const reg_t *reg);
void impl_compareValue(const memory_t *value);
void impl_compareSigned(const reg_t *reg);

/* I/O */

void impl_serialIn();
void impl_serialOut();

void impl_clrscr();
void impl_draw();

/* Branching */

int impl_jump(pair_t addr);

int impl_jmpz(pair_t addr);
int impl_jmpnz(pair_t addr);
int impl_jmpn(pair_t addr);
int impl_jmpnn(pair_t addr);
int impl_jmph(pair_t addr);
int impl_jmpnh(pair_t addr);
int impl_jmpc(pair_t addr);
int impl_jmpnc(pair_t addr);

#endif //CHIP9_EMULATOR_INSTRUCTION_IMPLEMENTATIONS_H
