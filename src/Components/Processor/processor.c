//
// Created by Fabian on 17.12.2019.
//

#include "processor.h"
#include "Instructions/instructions.h"
#include "../Memory/memory.h"
#include <stdlib.h>

bool processor_init() {
    //Remember: Little endian!
    reg_a = malloc(sizeof(uint8_t));

    reg_bc = malloc(sizeof(uint16_t));
    reg_b = (uint8_t *) reg_bc + sizeof(uint8_t);
    reg_c = (uint8_t *) reg_bc;

    reg_de = malloc(sizeof(uint16_t));
    reg_d = (uint8_t *) reg_de + sizeof(uint8_t);
    reg_e = (uint8_t *) reg_de;

    reg_hl = malloc(sizeof(uint16_t));
    reg_h = (uint8_t *) reg_hl + sizeof(uint8_t);
    reg_l = (uint8_t *) reg_hl;

    sp = malloc(sizeof(uint16_t));
    pc = malloc(sizeof(uint16_t));
    flags = malloc(sizeof(uint8_t));

    //Man 1.4.2
    *pc = 0;

    running = true;

    return true;
}

void processor_step() {
    *pc += executeInstruction() * sizeof(memory_t);
}

bool processor_stop() {
    free(reg_a);
    free(reg_bc);
    free(reg_de);
    free(reg_hl);

    return true;
}
