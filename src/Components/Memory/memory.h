//
// Created by Fabian on 17.12.2019.
//

#ifndef CHIP9_EMULATOR_MEMORY_H
#define CHIP9_EMULATOR_MEMORY_H

#include <stdint.h>
#include <stdbool.h>

#define MEMORY_SIZE 0xFFFF

typedef uint8_t memory_t;
memory_t* memory;

bool memory_init();
bool memory_stop();

bool memory_loadRom(char *fileName, int offset);

#endif //CHIP9_EMULATOR_MEMORY_H
