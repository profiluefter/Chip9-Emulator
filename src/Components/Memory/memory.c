//
// Created by Fabian on 17.12.2019.
//

#include "memory.h"
#include <stdlib.h>
#include <stdio.h>

bool memory_init() {
    memory = malloc(sizeof(memory_t) * MEMORY_SIZE);
    return true;
}

bool memory_stop() {
    free(memory);
    return true;
}

bool memory_loadRom(char *fileName, int offset) {
    FILE* fileHandle = fopen(fileName, "rb");

    fseek(fileHandle, 0L, SEEK_END);
    int romSize = ftell(fileHandle);
    rewind(fileHandle);

    memory_t* buffer = malloc(sizeof(memory_t) * romSize);
    fread(buffer, romSize, 1, fileHandle);

    for (int i = 0; i < romSize; ++i) {
        memory[offset + i] = buffer[i];
    }

    return true;
}
